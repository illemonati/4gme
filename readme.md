# 4gme

Downloads and displays past and current threads of /GME/ generals from 4chan's /biz/ board

[4gme.tong.icu](https://4gme.tong.icu)

## Technical Stack

### Backend

-   Multi-threaded Go server optimized for concurrent operations
-   RESTful API architecture
-   Integration with 4chan API for real-time thread data

### Frontend

-   React for dynamic user interface
-   Tailwind CSS with DaisyUI components for modern styling
-   Responsive design supporting multiple themes

### Deployment

-   Docker containerization for production environment
-   Multi-stage builds for optimized image size
-   Isolated runtime environment
-   Cross-platform compatibility

## Features

### Classic Theme

![Classic Theme](demo-assets/classic-threads.png)

### Cyber Theme

![Cyber Theme](demo-assets/cyber-threads.png)

### First Post

![First Post](demo-assets/cyber-firstpost.png)

### Specific Post Link

![Specific Post Link](demo-assets/cyber-link.png)

### Gallery View

![Gallery View](demo-assets/gallery.mov)
