package main

import (
	"gitlab.com/illemonati/4gme/fourchan"
	"gitlab.com/illemonati/4gme/web"
)

// Main Function
func main() {
	fc := fourchan.NewFourChan()
	go fc.StartUpdates(0)
	fc.Update()
	ws := web.NewWebServer(fc)
	ws.Run(":18423")
}
