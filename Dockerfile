## Build
FROM --platform=$BUILDPLATFORM golang:1.19-alpine AS build

WORKDIR /app

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY . ./

RUN GOOS=linux GOARCH=amd64 go build -v -ldflags "-s -w" -o /4gme

## Build node
FROM --platform=$BUILDPLATFORM node:19-alpine AS node-build

WORKDIR /webapp

COPY ./web/app/4gme-viewer/ ./

RUN yarn
RUN yarn build


## Deploy
FROM --platform=linux/amd64 alpine:latest

WORKDIR /

COPY --from=build /4gme /4gme
COPY --from=node-build /webapp/build/ /web/app/4gme-viewer/build/

EXPOSE 18423
VOLUME [ "/data/" ]

ENTRYPOINT [ "/4gme" ]