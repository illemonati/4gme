package fourchan

import (
	"encoding/json"
	"log"
	"os"
	"path"
	"sync"

	"gitlab.com/illemonati/4gme/config"
)

type ThreadIndex struct {
	sync.RWMutex
	FirstPosts map[int]*Post `json:"first_posts"`
}

var threadIndex *ThreadIndex

func GetIndexPath() string {
	return path.Join(config.DataPath, "4gme", "thread-index.json")
}

func (fc *FourChan) initThreadIndex() error {

	log.Println("init thread index")
	if threadIndex != nil {
		return nil
	}

	threadIndex = &ThreadIndex{
		FirstPosts: make(map[int]*Post),
	}

	file, err := os.Open(GetIndexPath())
	if err != nil {
		if !os.IsNotExist(err) {
			return err
		}

		log.Println("Thread index file not found, building from files...")

		posts, err := fc.GetAllThreadsFirstPosts("biz")
		if err != nil {
			return err
		}

		for _, post := range posts {
			threadIndex.FirstPosts[post.No] = post
		}

		return fc.saveThreadIndex()
	}
	defer file.Close()

	decoder := json.NewDecoder(file)
	return decoder.Decode(threadIndex)
}

func (fc *FourChan) saveThreadIndex() error {
	threadIndex.RLock()
	defer threadIndex.RUnlock()

	file, err := os.Create(GetIndexPath())
	if err != nil {
		return err
	}
	defer file.Close()

	encoder := json.NewEncoder(file)
	return encoder.Encode(threadIndex)
}

func (fc *FourChan) UpdateThreadInIndex(post *Post) error {
	if err := fc.initThreadIndex(); err != nil {
		return err
	}

	threadIndex.Lock()
	threadIndex.FirstPosts[post.No] = post
	threadIndex.Unlock()

	return fc.saveThreadIndex()
}

func (fc *FourChan) GetThreadIndex() ([]*Post, error) {
	if err := fc.initThreadIndex(); err != nil {
		return nil, err
	}

	threadIndex.RLock()
	defer threadIndex.RUnlock()

	posts := make([]*Post, 0, len(threadIndex.FirstPosts))
	for _, post := range threadIndex.FirstPosts {
		posts = append(posts, post)
	}
	return posts, nil
}
