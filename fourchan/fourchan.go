package fourchan

import (
	"sync"

	"github.com/go-resty/resty/v2"
)

// FourChan: 4chan interactor
type FourChan struct {
	sync.Mutex
	ActiveThreads Threads
	requestClient *resty.Client
	BaseURL       string
	BoardsBaseUrl string
	FileBaseUrl   string
}

func NewFourChan() *FourChan {
	fc := FourChan{
		ActiveThreads: []Thread{},
		requestClient: resty.New(),
		BaseURL:       "https://a.4cdn.org/",
		BoardsBaseUrl: "https://boards.4chan.org/",
		FileBaseUrl:   "https://i.4cdn.org/",
	}
	return &fc
}
