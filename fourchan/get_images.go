package fourchan

import (
	"fmt"
	"log"
	"net/url"
	"os"
	"path"
	"path/filepath"

	"gitlab.com/illemonati/4gme/config"
)

func (fc *FourChan) DownloadImages(threadPosts *ThreadPosts) {
	dir := GetThreadDir(threadPosts.BoardName, threadPosts.ThreadNo)
	for _, post := range threadPosts.Posts {
		if post.Filename != "" {
			filePath, err := fc.DownloadImage(&post, dir, threadPosts.BoardName)
			if err != nil {
				log.Printf("Downloading image %s failed: %v", filePath, err)
				continue
			}
			linkPath, err := CreateImageLink(threadPosts.BoardName, threadPosts.ThreadNo, filePath, &post)
			if err != nil {
				log.Printf("Create link failed for %s: %v", linkPath, err)
			}
		}
	}
}

func (fc *FourChan) DownloadImage(post *Post, dir string, boardName string) (filePath string, err error) {
	fileName := fmt.Sprintf("%s%s", post.Filename, post.Ext)
	filePath = path.Join(dir, "files", fmt.Sprint(post.Tim), fileName)

	// Tim is 4chan image id
	fileUrl, _ := url.JoinPath(fc.FileBaseUrl, boardName, fmt.Sprintf("%d%s", post.Tim, post.Ext))
	if _, e := os.Stat(filePath); e != nil {
		log.Printf("Downloading file: %s from %s", filePath, fileUrl)
		_, err = fc.requestClient.R().SetOutput(filePath).Get(fileUrl)
	}

	return
}

func CreateImageLink(board string, threadNo int, filePath string, post *Post) (linkPath string, err error) {
	dir := GetThreadLinksDir(board, threadNo)
	linkPath = filepath.Join(dir, fmt.Sprintf("%d-%s%s", post.Tim, post.Filename, post.Ext))
	if _, e := os.Stat(linkPath); e == nil {
		return
	}
	oldPath, err := filepath.Rel(dir, filePath)
	if err != nil {
		return
	}
	linkType := config.LinkType

	if linkType != "hardlink" {
		linkType = "symlink"
	}

	log.Printf("linking file with %s: %s -> %s", linkType, oldPath, linkPath)

	if linkType == "hardlink" {
		err = os.Link(filePath, linkPath)
	} else if linkType == "symlink" {
		err = os.Symlink(oldPath, linkPath)
	}
	return
}
