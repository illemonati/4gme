package fourchan

import (
	"log"
	"time"
)

func (fc *FourChan) StartUpdates(duration time.Duration) {
	if duration == 0 {
		duration = 30 * time.Second
	}
	ticker := time.NewTicker(duration)
	fc.Update()
	for range ticker.C {
		fc.Update()
	}
}

func (fc *FourChan) Update() {
	log.Printf("Starting update at %v", time.Now())
	err := fc.UpdateGme()
	if err != nil {
		log.Printf("Failed to update gme: %v", err)
	} else {
		log.Printf("Updated gme: %v", time.Now())
	}
	errs := fc.WriteActiveThreads()
	for _, err := range errs {
		log.Printf("Failed to write thread: %v", err)
	}
	log.Printf("Write active threads complete: %v", time.Now())
}
