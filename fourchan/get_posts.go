package fourchan

import (
	"encoding/json"
	"fmt"
	"log"
	"net/url"
)

func (fc *FourChan) GetThreadPosts(boardName string, threadNo int) (threadPosts *ThreadPosts, err error) {
	upath, err := url.JoinPath(fc.BaseURL, boardName, "thread", fmt.Sprintf("%d.json", threadNo))
	if err != nil {
		return
	}
	resp, err := fc.requestClient.R().SetResult(ThreadPosts{}).Get(upath)
	if err != nil {
		return
	}
	threadPosts = resp.Result().(*ThreadPosts)
	threadPosts.BoardName = boardName
	threadPosts.ThreadNo = threadNo
	return
}

func (fc *FourChan) WriteThreadPosts(boardName string, threadNo int) (err error) {
	posts, err := fc.GetThreadPosts(boardName, threadNo)
	if err != nil {
		return
	}

	if posts.Posts == nil || len(posts.Posts) == 0 {
		err = fmt.Errorf("posts is nil or empty")
		return
	}

	err = fc.UpdateThreadInIndex(&posts.Posts[0])
	if err != nil {
		log.Printf("Failed to update thread index: %v", err)
	}

	file, err := GetThreadJson(boardName, threadNo)
	if err != nil {
		return
	}
	defer file.Close()

	encoder := json.NewEncoder(file)
	encoder.Encode(posts)

	fc.DownloadImages(posts)

	return
}

func (fc *FourChan) WriteActiveThreads() (errs []error) {
	for _, thread := range fc.ActiveThreads {
		err := fc.WriteThreadPosts(thread.BoardName, thread.No)
		if err != nil {
			errs = append(errs, err)
		}
	}
	return
}
