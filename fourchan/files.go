package fourchan

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path"

	"gitlab.com/illemonati/4gme/config"
)

func GetBoardDir(board string) string {
	return path.Join(config.DataPath, "4gme", "thread-posts", board)
}

func GetThreadDir(board string, threadNo int) string {
	return path.Join(GetBoardDir(board), fmt.Sprint(threadNo))
}

func GetThreadLinksDir(board string, threadNo int) string {
	return path.Join(GetThreadDir(board, threadNo), "file-links")
}

func GetThreadJsonPath(board string, threadNo int) string {
	return path.Join(GetThreadDir(board, threadNo), "thread.json")
}

func GetThreadFileDirPath(board string, threadNo int, fileNo int) string {
	return path.Join(GetThreadDir(board, threadNo), "files", fmt.Sprint(fileNo))
}

func GetThreadFilePath(board string, threadNo int, fileNo int) (filePath string, err error) {
	dirPath := GetThreadFileDirPath(board, threadNo, fileNo)
	files, err := ioutil.ReadDir(dirPath)
	if err != nil {
		return
	}
	for _, file := range files {
		if !file.IsDir() {
			filePath = path.Join(dirPath, file.Name())
			return
		}
	}
	err = errors.New("no file found")
	return
}

func GetThreadJson(board string, threadNo int) (*os.File, error) {
	err := os.MkdirAll(GetThreadDir(board, threadNo), 0755)
	if err != nil {
		return nil, err
	}
	err = os.MkdirAll(GetThreadLinksDir(board, threadNo), 0755)
	if err != nil {
		return nil, err
	}
	return os.OpenFile(GetThreadJsonPath(board, threadNo), os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0755)
}
