package fourchan

import "fmt"

func (threads Threads) GetLatest() (latestThread *Thread, err error) {
	for i, thread := range threads {
		if latestThread == nil || (thread.No > latestThread.No) {
			latestThread = &threads[i]
		}
	}

	if latestThread == nil {
		err = fmt.Errorf("no threads found")
	}

	return
}
