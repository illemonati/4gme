package fourchan

import (
	"encoding/json"
	"os"
)

func ReadFirstPost(board string, threadNo int) (post *Post, err error) {
	jsonPath := GetThreadJsonPath(board, threadNo)
	jsonFile, err := os.Open(jsonPath)
	if err != nil {
		return
	}
	defer jsonFile.Close()
	decoder := json.NewDecoder(jsonFile)
	var c = []byte{0}
	for c[0] != '[' {
		_, err = jsonFile.Read(c)
		if err != nil {
			return
		}
	}
	decoder.Decode(&post)
	return
}
