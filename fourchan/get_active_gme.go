package fourchan

import (
	"net/url"
	"regexp"
)

func (fc *FourChan) GetBoards(boardName string) (boards *Boards, err error) {
	upath, err := url.JoinPath(fc.BaseURL, boardName, "catalog.json")
	if err != nil {
		return
	}
	resp, err := fc.requestClient.R().SetResult(Boards{}).Get(upath)
	if err != nil {
		return
	}
	boards = resp.Result().(*Boards)
	return
}

func (fc *FourChan) UpdateGme() (err error) {
	fc.Lock()
	defer fc.Unlock()
	biz, err := fc.GetBoards("biz")
	if err != nil {
		return
	}
	// r := regexp.MustCompile(`\/GME\/\s-\s.*`)
	r := regexp.MustCompile(`(?i)(^|\/|\s+|\-+|\++|)GME($|\/|\s+|\-+|\++)`)
	fc.ActiveThreads = []Thread{}
	for _, board := range *biz {
		for _, thread := range board.Threads {
			if r.MatchString(thread.Sub) {
				thread.BoardName = "biz"
				fc.ActiveThreads = append(fc.ActiveThreads, thread)
			}
		}
	}
	return
}
