package fourchan

type Boards []Board
type Threads []Thread
type Replies []Reply
type Posts []Post

// Board: Represents a 4chan board
type Board struct {
	Page    int     `json:"page"`
	Threads Threads `json:"threads"`
}

type Thread struct {
	No            int     `json:"no"`
	Sticky        int     `json:"sticky,omitempty"`
	Closed        int     `json:"closed,omitempty"`
	Now           string  `json:"now"`
	Name          string  `json:"name"`
	Sub           string  `json:"sub,omitempty"`
	Com           string  `json:"com,omitempty"`
	Filename      string  `json:"filename"`
	Ext           string  `json:"ext"`
	W             int     `json:"w"`
	H             int     `json:"h"`
	TnW           int     `json:"tn_w"`
	TnH           int     `json:"tn_h"`
	Tim           int64   `json:"tim"`
	Time          int     `json:"time"`
	Md5           string  `json:"md5"`
	Fsize         int     `json:"fsize"`
	Resto         int     `json:"resto"`
	ID            string  `json:"id"`
	Capcode       string  `json:"capcode,omitempty"`
	SemanticURL   string  `json:"semantic_url"`
	Replies       int     `json:"replies"`
	Images        int     `json:"images"`
	LastModified  int     `json:"last_modified"`
	LastReplies   Replies `json:"last_replies,omitempty"`
	Bumplimit     int     `json:"bumplimit,omitempty"`
	Imagelimit    int     `json:"imagelimit,omitempty"`
	OmittedPosts  int     `json:"omitted_posts,omitempty"`
	OmittedImages int     `json:"omitted_images,omitempty"`
	Trip          string  `json:"trip,omitempty"`
	BoardName     string  `json:"board_name,omitempty"`
}

type Reply struct {
	No      int    `json:"no"`
	Now     string `json:"now"`
	Name    string `json:"name"`
	Com     string `json:"com"`
	Time    int    `json:"time"`
	Resto   int    `json:"resto"`
	ID      string `json:"id"`
	Capcode string `json:"capcode"`
}

type Post struct {
	No          int    `json:"no"`
	Now         string `json:"now"`
	Name        string `json:"name"`
	Sub         string `json:"sub,omitempty"`
	Com         string `json:"com,omitempty"`
	Filename    string `json:"filename,omitempty"`
	Ext         string `json:"ext,omitempty"`
	W           int    `json:"w,omitempty"`
	H           int    `json:"h,omitempty"`
	TnW         int    `json:"tn_w,omitempty"`
	TnH         int    `json:"tn_h,omitempty"`
	Tim         int64  `json:"tim,omitempty"`
	Time        int    `json:"time"`
	Md5         string `json:"md5,omitempty"`
	Fsize       int    `json:"fsize,omitempty"`
	Resto       int    `json:"resto"`
	ID          string `json:"id"`
	Bumplimit   int    `json:"bumplimit,omitempty"`
	Imagelimit  int    `json:"imagelimit,omitempty"`
	SemanticURL string `json:"semantic_url,omitempty"`
	Replies     int    `json:"replies,omitempty"`
	Images      int    `json:"images,omitempty"`
	UniqueIps   int    `json:"unique_ips,omitempty"`
}

type ThreadPosts struct {
	Posts     Posts  `json:"posts"`
	ThreadNo  int    `json:"threads,omitempty"`
	BoardName string `json:"board_name,omitempty"`
}
