package fourchan

import (
	"os"
	"strconv"
	"sync"
)

// Gets a list of every thread in board
func (*FourChan) GetAllThreadsFirstPosts(board string) (posts []*Post, err error) {
	boardDir := GetBoardDir(board)
	boardDirFile, err := os.Open(boardDir)
	if err != nil {
		return
	}
	defer boardDirFile.Close()
	possibleThreadNos, _ := boardDirFile.Readdirnames(0)
	postsChan := make(chan *Post)
	wg := new(sync.WaitGroup)
	for _, possibleThreadNo := range possibleThreadNos {
		wg.Add(1)
		go processSingleThread(board, possibleThreadNo, postsChan, wg)
	}
	go func() {
		wg.Wait()
		close(postsChan)
	}()
	for post := range postsChan {
		posts = append(posts, post)
	}
	return
}

func processSingleThread(board string, possibleThreadNo string, postsChan chan *Post, wg *sync.WaitGroup) (err error) {
	defer wg.Done()
	intThreadNo, err := strconv.Atoi(possibleThreadNo)
	if err != nil {
		return
	}
	threadFirstPost, err := ReadFirstPost(board, intThreadNo)
	if err != nil {
		return
	}
	postsChan <- threadFirstPost

	return
}
