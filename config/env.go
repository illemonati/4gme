package config

import "os"

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

// either "hardlink" or "symlink"
var LinkType string = getEnv("LINK_TYPE", "symlink")

// Data Path
var DataPath string = getEnv("DATA_PATH", "/data")
