package web

import (
	"strings"

	"github.com/gin-gonic/gin"
)

func (ws *WebServer) addStatic() {
	// ws.router.NoRoute(gin.WrapH(http.FileServer(http.Dir("./web/app/4gme-viewer/build"))))
	// ws.router.NoRoute(func(c *gin.Context) {
	// 	c.File("./web/app/4gme-viewer/build/index.html"
	// })
	ws.router.Static("/app/", "./web/app/4gme-viewer/build")
	ws.router.NoRoute(func(c *gin.Context) {
		path := c.Request.URL.String()
		if strings.HasPrefix(path, "/app") {
			c.File("./web/app/4gme-viewer/build/index.html")
		} else {
			c.Redirect(302, "/app/")
		}
	})
}
