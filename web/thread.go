package web

import (
	"fmt"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/illemonati/4gme/fourchan"
)

func (ws *WebServer) addThread() {
	ws.apiRouter.GET("/thread/:no", func(c *gin.Context) {
		noStr := c.Param("no")
		no, err := strconv.Atoi(noStr)
		if err != nil {
			c.AbortWithError(500, fmt.Errorf("error converting no: %v", err))
			return
		}
		threadPostsFilePath := fourchan.GetThreadJsonPath("biz", no)
		c.File(threadPostsFilePath)
	})
}
