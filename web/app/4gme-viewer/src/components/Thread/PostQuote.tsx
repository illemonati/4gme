import React from "react";

interface PostQuoteProps {
    children: string;
}

export const PostQuote: React.FC<PostQuoteProps> = ({ children }) => (
    <span className="text-success font-medium">{children}</span>
);
