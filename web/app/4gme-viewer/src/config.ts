const NORMAL_BASE =
    process.env.NODE_ENV === "production"
        ? "/api/"
        : "http://localhost:18423/api/";

export const API_BASE = process.env.REACT_APP_API_BASE || NORMAL_BASE;
