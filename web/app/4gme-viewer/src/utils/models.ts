export type Posts = Post[];
export type Replies = Reply[];
export interface Thread {
    no: number;
    sticky?: number;
    closed?: number;
    now: string;
    name: string;
    sub?: string;
    com?: string;
    filename: string;
    ext: string;
    w: number;
    h: number;
    tn_w: number;
    tn_h: number;
    tim: number;
    time: number;
    md5: string;
    fsize: number;
    resto: number;
    id: string;
    capcode?: string;
    semantic_url: string;
    replies: number;
    images: number;
    last_modified: number;
    last_replies?: Replies;
    bumplimit?: number;
    imagelimit?: number;
    omitted_posts?: number;
    omitted_images?: number;
    trip?: string;
    board_name?: string;
}

export interface Reply {
    no: number;
    now: string;
    name: string;
    com: string;
    time: number;
    resto: number;
    id: string;
    capcode: string;
}

export interface Post {
    no: number;
    now: string;
    name: string;
    sub?: string;
    com?: string;
    filename?: string;
    ext?: string;
    w?: number;
    h?: number;
    tn_w?: number;
    tn_h?: number;
    tim?: number;
    time: number;
    md5?: string;
    fsize?: number;
    resto: number;
    id: string;
    bumplimit?: number;
    imagelimit?: number;
    semantic_url?: string;
    replies?: number;
    images?: number;
    unique_ips?: number;
    is_non_standard?: boolean; // custom attribute for non standard /GME/ - editions
}

export interface ThreadPosts {
    posts: Posts;
    threads?: number;
    board_name?: string;
}
