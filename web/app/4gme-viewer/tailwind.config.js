/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        "./src/**/*.{js,jsx,ts,tsx}",
        "./node_modules/react-tailwindcss-datepicker/dist/index.esm.js",
    ],
    theme: {
        extend: {},
    },
    plugins: [require("daisyui")],
    daisyui: {
        themes: [
            {
                cyber4chan: {
                    primary: "#00ff2a",
                    secondary: "#ff4300",
                    accent: "#8b6dff",
                    neutral: "#0a0d12",
                    "base-100": "#12151a",
                    "base-200": "#1a1f26",
                    "base-300": "#232831",
                    info: "#00ccff",
                    success: "#00ff2a",
                    warning: "#ff4300",
                    error: "#ff2147",

                    "--neutral-content": "#ecf0ff",
                    "--base-content": "#ecf0ff",

                    "--rounded-box": "0",
                    "--rounded-btn": "0",
                    "--rounded-badge": "0",

                    ".border": {
                        "border-color": "#232831",
                    },

                    "--btn-text-case": "none",
                    "--animation-btn": "0.25s",
                    "--animation-input": "0.2s",

                    "h1, h2, h3, h4": {
                        "text-shadow": "0 0 10px rgba(0, 204, 255, 0.5)",
                    },

                    ".card": {
                        transition: "transform 0.2s ease-in-out",
                        "&:hover": {
                            transform: "translateY(-2px)",
                            "box-shadow": "0 0 20px rgba(139, 109, 255, 0.2)",
                        },
                    },

                    "a:hover": {
                        "text-shadow": "0 0 8px rgba(0, 255, 42, 0.8)",
                    },

                    "[data-theme='cyber4chan'] .navbar": {
                        "clip-path":
                            "polygon(0 0, 100% 0, 99.25% 100%, 0.75% 100%)",
                        "border-bottom": "2px solid #232831",
                        "box-shadow": "0 0 10px rgba(0, 204, 255, 0.2)",
                        position: "relative",
                        "&::after": {
                            content: '""',
                            position: "absolute",
                            bottom: "0",
                            left: "0",
                            right: "0",
                            height: "2px",
                            background:
                                "linear-gradient(90deg, transparent, #00ccff, transparent)",
                            opacity: "0.6",
                        },
                    },
                },
            },
            {
                classic4chan: {
                    primary: "#789922", // 4chan's quote green
                    secondary: "#FF6600", // 4chan's orange
                    accent: "#34345C", // traditional 4chan blue
                    neutral: "#D6DAF0", // light blue-gray
                    "base-100": "#EEF2FF", // classic 4chan background
                    "base-200": "#D6DAF0", // slightly darker background
                    "base-300": "#B7C5D9", // border color
                    info: "#34345C", // blue
                    success: "#789922", // green
                    warning: "#FF6600", // orange
                    error: "#FF0000", // red

                    "--rounded-box": "1rem",
                    "--rounded-btn": "0.5rem",
                    "--rounded-badge": "1.9rem",

                    "--animation-btn": "0.25s",
                    "--animation-input": "0.2s",

                    "--btn-text-case": "uppercase",
                    "--btn-focus-scale": "0.95",

                    "--border-btn": "1px",
                    "--tab-border": "1px",
                    "--tab-radius": "0.5rem",

                    ".navbar": {
                        "border-radius": "1rem",
                    },
                },
            },
            "light",
            "dark",
        ],
    },
};
