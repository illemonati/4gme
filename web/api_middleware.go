package web

import (
	"strings"

	"github.com/gin-gonic/gin"
)

func APIMiddleWare() gin.HandlerFunc {
	return func(c *gin.Context) {
		if strings.HasPrefix(c.Request.URL.String(), "/api") {
			if strings.HasPrefix(c.Request.URL.String(), "/api/file") {
				c.Header("Cache-Control", "max-age=31536000")
			} else {
				c.Header("Cache-Control", "no-cache, no-store, must-revalidate")
				c.Header("Pragma", "no-cache")
				c.Header("Expires", "0")
			}
		}
		c.Next()
	}
}

func (ws *WebServer) useAPIMiddleWare() {
	ws.apiRouter.Use(APIMiddleWare())
}
