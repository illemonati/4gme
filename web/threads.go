package web

import (
	"fmt"

	"github.com/gin-gonic/gin"
)

func (ws *WebServer) addThreads() {
	ws.apiRouter.GET("/threads", func(c *gin.Context) {
		threadPosts, err := ws.fc.GetAllThreadsFirstPosts("biz")
		if err != nil {
			c.AbortWithError(500, fmt.Errorf("error getting thread posts: %v", err))
			return
		}
		for _, post := range threadPosts {
			post.Com = ""
		}
		c.JSON(200, threadPosts)
	})
}
