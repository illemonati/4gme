package web

import (
	"fmt"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/illemonati/4gme/fourchan"
)

func (ws *WebServer) addFile() {
	ws.apiRouter.GET("/file/:threadNo/:fileNo", func(c *gin.Context) {
		threadNoStr := c.Param("threadNo")
		threadNo, err := strconv.Atoi(threadNoStr)
		filename := c.Query("filename")
		if err != nil {
			c.AbortWithError(500, fmt.Errorf("error converting threadNo: %v", err))
			return
		}
		fileNoStr := c.Param("fileNo")
		fileNo, err := strconv.Atoi(fileNoStr)
		if err != nil {
			c.AbortWithError(500, fmt.Errorf("error converting fileNo: %v", err))
			return
		}
		filePath, err := fourchan.GetThreadFilePath("biz", threadNo, fileNo)
		if err != nil {
			c.AbortWithError(500, fmt.Errorf("error getting filepath: %v", err))
			return
		}
		if len(filename) < 1 {
			c.File(filePath)
		} else {
			c.FileAttachment(filePath, filename)
		}
	})
}
