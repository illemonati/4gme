package web

import (
	"github.com/gin-gonic/gin"
	cors "github.com/rs/cors/wrapper/gin"
	"gitlab.com/illemonati/4gme/fourchan"
)

type WebServer struct {
	router    *gin.Engine
	apiRouter *gin.RouterGroup
	fc        *fourchan.FourChan
}

func NewWebServer(fc *fourchan.FourChan) *WebServer {
	ws := new(WebServer)
	ws.fc = fc
	ws.router = gin.Default()
	ws.apiRouter = ws.router.Group("/api")

	ws.router.Use(cors.Default())
	ws.useAPIMiddleWare()
	ws.addLive()
	ws.addThreads()
	ws.addThread()
	ws.addFile()
	ws.addStatic()
	return ws
}

func (ws *WebServer) Run(addr ...string) error {
	return ws.router.Run(addr...)
}
