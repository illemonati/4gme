package web

import (
	"fmt"
	"net/url"

	"github.com/gin-gonic/gin"
)

func (ws *WebServer) addLive() {
	ws.router.GET("/live", func(c *gin.Context) {
		ws.fc.Lock()
		defer ws.fc.Unlock()
		latestThread, err := ws.fc.ActiveThreads.GetLatest()
		if err != nil {
			c.AbortWithError(500, fmt.Errorf("error getting latest thread: %v", err))
			return
		}
		p, err := url.JoinPath(ws.fc.BoardsBaseUrl, "biz", "thread", fmt.Sprint(latestThread.No))
		if err != nil {
			c.AbortWithError(500, fmt.Errorf("error joining path: %v", err))
			return
		}
		c.Redirect(302, p)
	})
}
